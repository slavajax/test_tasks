<?php

use \Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();

/**
 * Добавление пользователя при регистрации в группу "Физические лица",
 * либо "Партнеры".
 */
$eventManager->addEventHandler('main', 'OnBeforeUserAdd', 'OnBeforeUserAddHandler');
function OnBeforeUserAddHandler(&$arFields)
{
    //Получить id групп пользователей
    $groupsData = [];
    $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", ["STRING_ID" => "INDIVIDUALS|PARTNERS"]);
    while ($arGroups = $rsGroups->Fetch()) {
        $groupsData[$arGroups['STRING_ID']] = $arGroups['ID'];
    }

    if ($arFields['UF_NAME_LEGAL_ENTITY'] == 'NONE'
        && $arFields['UF_INN'] == 'NONE'
        && $arFields['UF_PARTNER_ID'] == 'NONE') {
        $arFields['GROUP_ID'][] = $groupsData['INDIVIDUALS'];
    } else {
        $arFields['GROUP_ID'][] = $groupsData['PARTNERS'];
    }
}