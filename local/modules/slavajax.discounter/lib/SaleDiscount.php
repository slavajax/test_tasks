<?php

namespace slavajax\Discounter;

use Bitrix\Main\Application;
use Bitrix\Main\Diag\Debug;
use slavajax\Discounter\CouponDiscount;

class SaleDiscount
{
    protected static $_instance = [];

    public static function getInstance()
    {
        $calledClass = get_called_class();
        if (!isset(self::$_instance[$calledClass])) {
            self::$_instance[$calledClass] = new self();
        }
        return self::$_instance[$calledClass];
    }


    /**
     * Добавить новое правило корзины
     * @param int $discountValue
     * @param array $userGroups
     */
    public function add(int $discountValue, array $userGroups = [])
    {

        global $APPLICATION;

        $arDiscountFields = [
            'LID' => SITE_ID,
            'SITE_ID' => SITE_ID,
            'NAME' => sprintf("Скидка для партнеров %s%%", $discountValue),
            'DISCOUNT_VALUE' => $discountValue,
            'DISCOUNT_TYPE' => 'P',
            'LAST_LEVEL_DISCOUNT' => 'Y',
            'LAST_DISCOUNT' => 'Y',
            'ACTIVE' => 'Y',
            'CURRENCY' => 'RUR',
            'USER_GROUPS' => $userGroups,
            'COUPON_ADD' => 'Y',
            'COUPON_COUNT' => 1,
            'COUPON' => [
                'TYPE' => 'Y',
                'ACTIVE_FROM' => null,
                'ACTIVE_TO' => null,
                'MAX_USE' => 0
            ],
            'ACTIONS' => [
                'CLASS_ID' => 'CondGroup',
                'DATA' => [
                    'All' => 'AND'
                ],
                'CHILDREN' => [
                    [
                        'CLASS_ID' => 'ActSaleBsktGrp',
                        'DATA' => [
                            'Type' => 'Discount',
                            'Value' => $discountValue,
                            'Unit' => 'Perc',
                            'Max' => 0,
                            'All' => 'OR',
                            'True' => 'True',
                        ]
                    ]
                ]
            ],
            'CONDITIONS' => [
                'CLASS_ID' => 'CondGroup',
                'DATA' => [
                    'All' => 'AND',
                    'True' => 'True',
                ]
            ]
        ];

        $discountId = \CSaleDiscount::Add($arDiscountFields);

        if ($discountId) {
            CouponDiscount::getInstance()->add($discountId);
        } else {
            $ex = $APPLICATION->GetException();
            Debug::dumpToFile($ex, '', 'mylog' . time() . '.txt');
        }
    }


    /**
     * Возвращает список правил работы с корзиной
     * с ограничением по группе пользователей "Партнеры"
     * @return array
     */
    public function getList(int $groupId)
    {
        $discounts = [];

        $dbResult = \CSaleDiscount::GetList(
            ['SORT' => 'ASC'],
            ['ACTIVE' => 'Y', 'USER_GROUPS' => $groupId],
            false,
            false,
            ['ID', 'NAME', 'ACTIONS']
        );

        while ($arResult = $dbResult->Fetch()) {
            $discounts[current(unserialize($arResult['ACTIONS'])['CHILDREN'])['DATA']['Value']] = $arResult['ID'];
        }

        return $discounts;
    }
}