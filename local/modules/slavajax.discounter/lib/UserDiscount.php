<?php

namespace slavajax\Discounter;

use Bitrix\Main\UserTable;

class UserDiscount
{
    protected static $_instance = [];

    public static function getInstance()
    {
        $calledClass = get_called_class();
        if (!isset(self::$_instance[$calledClass])) {
            self::$_instance[$calledClass] = new self();
        }
        return self::$_instance[$calledClass];
    }


    /**
     * Получить список размера скидок для
     * каждого пользователя
     * @param int $groupId
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getList(int $groupId)
    {
        $users = [];

        $result = \Bitrix\Main\UserGroupTable::getList([
            'select' => ['USER_ID', 'PERSONAL_DISCOUNT' => 'USER.UF_PERSONAL_DISCOUNT'],
            'filter' => ['GROUP_ID' => $groupId, 'USER.ACTIVE' => 'Y']
        ]);
        while ($arGroup = $result->fetch()) {
            $users[] = $arGroup;
        }

        return $users;
    }
}