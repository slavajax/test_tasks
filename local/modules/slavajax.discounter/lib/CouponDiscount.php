<?php

namespace slavajax\Discounter;

use Bitrix\Main\Diag\Debug;
use \Bitrix\Sale\DiscountCouponsManager;
use \Bitrix\Sale\Internals\DiscountCouponTable;

class CouponDiscount
{
    protected static $_instance = [];

    public static function getInstance()
    {
        $calledClass = get_called_class();
        if (!isset(self::$_instance[$calledClass])) {
            self::$_instance[$calledClass] = new self();
        }
        return self::$_instance[$calledClass];
    }


    /**
     * Генерация купона из 8 символов
     * @return string
     */
    public function generate()
    {
        $allchars = 'ABCDEFGHIJKLNMOPQRSTUVWXYZ0123456789';
        $charsLen = mb_strlen($allchars) - 1;
        $string = '';

        for ($i = 0; $i < 8; $i++)
            $string .= mb_substr($allchars, rand(0, $charsLen), 1);

        return $string;
    }


    /**
     * Существует ли купон в системе?
     * @param $coupon
     * @return bool
     */
    public function isExist($coupon)
    {
        return DiscountCouponsManager::isExist($coupon) ? true : false;
    }


    /**
     * Добавление нового уникального купона
     * @param int $discountId
     * @param int $userId
     * @throws \Exception
     */
    public function add(int $discountId, int $userId = 0)
    {
        do {
            $resultCorrect = true;
            $coupon = $this->generate();

            $existCoupon = $this->isExist($coupon);
            $resultCorrect = empty($existCoupon);

        } while (!$resultCorrect);

        $couponFields = [
            'DISCOUNT_ID' => $discountId,
            'COUPON' => $coupon,
            'ACTIVE' => 'Y',
            'TYPE' => 4,
            'MAX_USE' => 0
        ];

        if ($userId)
            $couponFields['USER_ID'] = $userId;

        $couponAddResult = DiscountCouponTable::add($couponFields);
    }


    /**
     * Проверка существования купона по user_id
     * @param int $userId
     * @return bool|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function checkUserCoupon(int $userId)
    {
        $arCoupon = DiscountCouponTable::getRow([
            'filter' => ['USER_ID' => $userId, 'ACTIVE' => 'Y']
        ]);

        if (is_array($arCoupon)) {
            return [
                'COUPON_ID' => $arCoupon['ID'],
                'DISCOUNT_ID' => $arCoupon['DISCOUNT_ID']
            ];
        }

        return false;
    }


    /**
     * Деактивировать купон
     * @param int $couponId
     * @throws \Exception
     */
    public function deactivate(int $couponId)
    {
        DiscountCouponTable::update($couponId, ['ACTIVE' => 'N']);
    }
}