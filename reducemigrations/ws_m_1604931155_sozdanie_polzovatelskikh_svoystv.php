<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1604931155_sozdanie_polzovatelskikh_svoystv extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание пользовательских свойств";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "b297b8959d9acd1b2cdb009860cf2b7fdd78c678";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 2;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        $oUserTypeEntity = new CUserTypeEntity();

        //Пользовательское свойство "Название юр.лица"
        $aUserFields = [
            'ENTITY_ID' => 'USER',
            'FIELD_NAME' => 'UF_NAME_LEGAL_ENTITY',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => 'XML_ID_NAME_LEGAL_ENTITY',
            'SORT' => 500,
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => 'NONE',
                'SIZE' => 20,
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Название юр.лица',
                'en' => 'Name legal entity'
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Название юр.лица',
                'en' => 'Name legal entity'
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Название юр.лица',
                'en' => 'Name legal entity'
            ],
            'ERROR_MESSAGE' => [
                'ru' => 'Ошибка при заполнении пользовательского свойства',
                'en' => 'An error in completing the user field'
            ],
            'HELP_MESSAGE' => [
                'ru' => '',
                'en' => ''
            ]
        ];
        $oUserTypeEntity->Add($aUserFields);

        //Пользовательское свойство "ИНН"
        $aUserFields = [
            'ENTITY_ID' => 'USER',
            'FIELD_NAME' => 'UF_INN',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => 'XML_ID_INN',
            'SORT' => 600,
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => 'NONE',
                'SIZE' => 20,
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'ИНН',
                'en' => 'INN'
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'ИНН',
                'en' => 'INN'
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'ИНН',
                'en' => 'INN'
            ],
            'ERROR_MESSAGE' => [
                'ru' => 'Ошибка при заполнении пользовательского свойства',
                'en' => 'An error in completing the user field'
            ],
            'HELP_MESSAGE' => [
                'ru' => '',
                'en' => ''
            ]
        ];
        $oUserTypeEntity->Add($aUserFields);

        //Пользовательское свойство "Идентификатор партнера"
        $aUserFields = [
            'ENTITY_ID' => 'USER',
            'FIELD_NAME' => 'UF_PARTNER_ID',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => 'XML_ID_PARTNER_ID',
            'SORT' => 700,
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => 'NONE',
                'SIZE' => 20,
                'ROWS' => 1,
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'REGEXP' => ''
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Идентификатор партнера',
                'en' => 'Partner id'
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Идентификатор партнера',
                'en' => 'Partner id'
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Идентификатор партнера',
                'en' => 'Partner id'
            ],
            'ERROR_MESSAGE' => [
                'ru' => 'Ошибка при заполнении пользовательского свойства',
                'en' => 'An error in completing the user field'
            ],
            'HELP_MESSAGE' => [
                'ru' => '',
                'en' => ''
            ]
        ];
        $oUserTypeEntity->Add( $aUserFields );
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        //Инициализация id свойств пользователя
        $userPropertiesIds = ['UF_NAME_LEGAL_ENTITY' => 0, 'UF_INN' => 0, 'UF_PARTNER_ID' => 0];
        //Пользовательские свойства пользователя
        $userProperties = ['UF_NAME_LEGAL_ENTITY', 'UF_INN', 'UF_PARTNER_ID'];

        //Сбор id свойств
        $rsData = CUserTypeEntity::GetList([], ['ENTITY_ID' => 'USER']);
        while ($arResult = $rsData->Fetch()) {
            if (in_array($arResult['FIELD_NAME'], $userProperties)) {
                $userPropertiesIds[$arResult['FIELD_NAME']] = $arResult['ID'];
            }
        }

        //Удаление свойств пользователя
        $oUserTypeEntity = new CUserTypeEntity();
        foreach ($userPropertiesIds as $userPropertyId) {
            $oUserTypeEntity->Delete($userPropertyId);
        }
    }
}