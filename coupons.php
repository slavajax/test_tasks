<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Генерация купонов для пользователей");
?>
<?php

\Bitrix\Main\Loader::includeModule('slavajax.discounter');

use slavajax\Discounter\ToolsDiscount;
use slavajax\Discounter\SaleDiscount;
use slavajax\Discounter\UserDiscount;
use slavajax\Discounter\CouponDiscount;

$saleDiscountInstance = SaleDiscount::getInstance();
$toolsDiscountInstance = ToolsDiscount::getInstance();
$userDiscountInstance = UserDiscount::getInstance();
$couponDiscountInstance = CouponDiscount::getInstance();

//Id группы партнеры
$groupPartnersId = $toolsDiscountInstance->getPartnersGroupId();

//Список пользователей, для которых нужно создать купоны
$usersDiscount = $userDiscountInstance->getList($groupPartnersId);

//Список правил со скидками пользователей
foreach ($usersDiscount as $userDiscount) {
    $usersDiscountValues[] = $userDiscount['PERSONAL_DISCOUNT'];
}

//Список доступных правил работы с корзиной
$saleDiscounts = $saleDiscountInstance->getList($groupPartnersId);

//Создать правила работы с корзиной, если таковых нет
//Проверяется на основе полученных ранее значений от пользователей
foreach ($usersDiscountValues as $userDiscountValue) {
    if (!array_key_exists($userDiscountValue, $saleDiscounts)) {
        $saleDiscountInstance->add($userDiscountValue, [$groupPartnersId]);
    }
}

if (!$saleDiscounts) {
    $saleDiscounts = $saleDiscountInstance->getList($groupPartnersId);
}

//Создать купоны для пользователей
//Если купон у пользователя существует, то отключить
//и создать новый
foreach ($usersDiscount as $userDiscount) {
    $arCoupon = $couponDiscountInstance->checkUserCoupon($userDiscount['USER_ID']);

    if (is_array($arCoupon)) {
        $couponDiscountInstance->deactivate($arCoupon['COUPON_ID']);
        $couponDiscountInstance->add($arCoupon['DISCOUNT_ID'], $userDiscount['USER_ID']);
    } else {
        $couponDiscountInstance->add($saleDiscounts[$userDiscount['PERSONAL_DISCOUNT']], $userDiscount['USER_ID']);
    }
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>